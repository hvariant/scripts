#!/bin/env python3

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow.python.client import device_lib

def get_available_devices():
    local_device_protos = device_lib.list_local_devices()
    #return [x.name for x in local_device_protos]
    return local_device_protos

for name in get_available_devices():
    print(name)
