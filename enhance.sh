#!/bin/bash

# options help
#docker run --rm -v `pwd`:/ne/input -it alexjc/neural-enhance:gpu --help

# gpu version
#nvidia-docker run --rm -v `pwd`:/ne/input -it alexjc/neural-enhance:gpu --model=default --zoom=2 --device=gpu /ne/input/$1
#nvidia-docker run --rm -v `pwd`:/ne/input -it alexjc/neural-enhance:gpu --model=default --zoom=4 --device=gpu /ne/input/$1
#nvidia-docker run --rm -v `pwd`:/ne/input -it alexjc/neural-enhance:gpu --model=deblur --zoom=1 --device=gpu /ne/input/$1
#nvidia-docker run --rm -v `pwd`:/ne/input -it alexjc/neural-enhance:gpu --model=repair --zoom=1 --device=gpu /ne/input/$1

# waifu2x
nvidia-docker run --rm -p 8812:8812 -v $HOME/.cache/waifu2x/ComputeCache:/root/.nv/ComputeCache \
    -v `pwd`:/root/sharedfolder/ \
    -it hvariant/waifu2x
