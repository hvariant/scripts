#!/bin/bash

WINDOW=$1

while :
do
    xdotool keydown --window $WINDOW space
    sleep 0.12
    xdotool keyup --window $WINDOW space
    sleep 0.12
done
