#!/bin/bash

N=30

for i in `seq $N`; do
    sleep 1

    if [[ "$?" -ne 0 ]]; then 
        break
    fi

    echo $i;
done
