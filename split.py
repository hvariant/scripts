#!env python3

from os import listdir
from os.path import isfile, join
import numpy as np
from pprint import pprint
from subprocess import check_output

path = "test_mask_hq"
pattern = "part_{}"
n = 10

onlyfiles = np.array([join(path,f) for f in listdir(path) if isfile(join(path, f))])
splits = np.array_split(onlyfiles, n)

# for i,files in enumerate(splits):
    # folder = pattern.format(i+1)
    # check_output(["mkdir", "-p", folder])
    # for p in files:
        # check_output(["cp", p, folder])

for i in range(n):
    folder = pattern.format(i+1)
    check_output(["zip", "-r", folder + ".zip", folder])
