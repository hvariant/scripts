#!/bin/bash

ffmpeg -i $1 -vn -ar 44100 -ac 2 -ab 320k -f mp3 $2
