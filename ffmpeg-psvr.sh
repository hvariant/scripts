#!/bin/bash

ffmpeg -i $1 -vcodec libx264 -profile:v high -level:v 4.2 -s 1920x1080 -preset slow -crf 18 -c:a aac -vbr 5 $2.mp4

