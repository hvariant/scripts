#!/bin/bash

sudo pacman -Syu
sudo aura -Ayu
sudo pip-review --interactive
npm update -g

nvim +PlugInstall +qa
