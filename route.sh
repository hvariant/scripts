#!/bin/sh

dotest () {
  printf '%s: ' "$1" ; shift
  printf ' %s' "$@" ; printf '\n'
  "${@}" 2>&1 | sed 's/^/    /g'
}

hostck () {
  for h in "$@" ; do
    dotest "IP Nexthop"   ip -o -4 rout get ${h}
    dotest "INET Valid"   ping -nq -l3 -c3 ${h}
    #wrap "INET Route"   mtr -nrc1 ${host}
  done
}

dotest "IPv4 IFaces"  ip -o -4 ad sh | sed 's,\\ .\+$,,'
dotest "IPv4 Routes"  ip -o -4 ro sh
dotest "DNS Config"   cat /etc/resolv.conf | sed '/^[ \t]*#/d;/^[ \t]*$/d;'

hostck 8.8.8.8

