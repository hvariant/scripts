#!/bin/bash

docker pull pytorch/pytorch:latest
docker pull hvariant/torch7:latest
docker pull hvariant/waifu2x:latest
docker pull hvariant/rethinking:latest
docker pull bvlc/caffe:gpu
