#!/bin/bash

set -x

serverb="<remote-server-address>"
user="<user-on-remote-server>"
port="<local-machine-ssh-port>"
port_remote="<remote-machine-ssh-port>"

autossh -M 20000 -v -o ExitOnForwardFailure=yes -N -f -R 0.0.0.0:${port_remote}:localhost:${port} ${user}@$serverb
