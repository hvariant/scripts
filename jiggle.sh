#!/bin/bash

# reference: http://unix.stackexchange.com/questions/91249/script-to-prevent-screen-blanking-using-mouse-move-doesnt-work

sleep_period=5

while true; do
    #xset -dpms; xset s off
    #xset +dpms; xset s on

    xdotool mousemove_relative 10 10
    xdotool mousemove_relative -- -10 -10
    sleep ${sleep_period}
done
