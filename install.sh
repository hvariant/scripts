#!/bin/sh

git config --global core.editor vim
git config --global user.name "Jason Li"
git config --global user.email "lizhansong@hotmail.com"

#git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
wget https://raw.githubusercontent.com/hvariant/dotfiles/master/vimrc_minimal -O ~/.vimrc
#vim +PluginInstall +qall
