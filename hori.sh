#!/bin/bash

sudo xboxdrv \
    --evdev /dev/input/by-id/usb-HORI_CO._LTD._Fighting_Stick_mini_4-event-joystick \
    --mimic-xpad                                     \
    --evdev-no-grab                                  \
    --evdev-absmap ABS_HAT0X=dpad_x,ABS_HAT0Y=dpad_y \
    --evdev-debug                                    \
    --evdev-keymap BTN_SOUTH=x,BTN_EAST=a            \
    --evdev-keymap BTN_C=b,BTN_NORTH=y               \
    --evdev-keymap BTN_WEST=lb,BTN_Z=rb              \
    --evdev-keymap BTN_TL=lt,BTN_TR=rt               \
    --evdev-keymap BTN_MODE=guide                    \
    --evdev-keymap BTN_TL2=back,BTN_TR2=start        \
    #--silent                                         \
