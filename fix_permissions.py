#!/bin/env python3

import os
import magic
import grp
import pwd
import argparse
import getpass

parser = argparse.ArgumentParser(description="fix permission problems with Microsoft's evil NTFS filesystem")
parser.add_argument("root_dir")
parser.add_argument("-u","--user",help="owner user name",type=str)
parser.add_argument("-g","--group",help="owner group name",type=str)

args = parser.parse_args()
root_dir = args.root_dir

print(args.user)

if args.user:
    user = args.user
else:
    user = getpass.getuser()

if args.group:
    group = args.group
else:
    group = None

def is_executable(path):
    t = magic.from_file(path)
    print("%s:%s" % (path,t))

    if t.find("executable") >= 0: #roughly true
        return True

    return False

def fix_perm(path, mode, uid, gid):
    os.chmod(path,mode)
    os.chown(path,uid,gid)

dir_default_mode = 0o755
file_default_mode = 0o644
exec_default_mode = 0o775

uid = pwd.getpwnam(user).pw_uid
gid = pwd.getpwnam(user).pw_gid
if group is not None:
    gid = grp.getgrnam(group).gr_gid

fix_perm(root_dir,dir_default_mode,uid,gid)

for root, dirs, files in os.walk(root_dir):
    print("==="*10)
    print("R:%s" % root)

    for dn in dirs:
        path = os.path.join(root,dn)
        fix_perm(path,dir_default_mode,uid,gid)

    for fn in files:
        path = os.path.join(root,fn)
        if is_executable(path):
            fix_perm(path,exec_default_mode,uid,gid)
        else:
            fix_perm(path,file_default_mode,uid,gid)
